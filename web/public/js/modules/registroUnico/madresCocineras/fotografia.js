// Put event listeners into place
function configCamera(){
    var canvasArea = "canvas";
    var videoArea = "video";
    // Grab elements, create settings, etc.
    var canvas = document.getElementById(canvasArea),
        context = canvas.getContext("2d"),
        video = document.getElementById(videoArea),
        videoObj = {"video": true},
        errBack = function (error) {
            //showNotify("Error en la Captura de Video", error.code);
            console.log("Video capture error:", error.code);
        };
    // Put video listeners into place
    if (navigator.getUserMedia) { // Standard
        navigator.getUserMedia(videoObj, function (stream) {
            video.src = stream;
            video.play();
        }, errBack);
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(videoObj, function (stream) {
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
        }, errBack);
    }
    else if (navigator.mozGetUserMedia) { // Firefox-prefixed
        navigator.mozGetUserMedia(videoObj, function (stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        }, errBack);
    }
    
    var foto = $("#TalentoHumano_foto").val();
    
    if(foto.length==0){
        
    }
    
    buttonsEvents(video, videoObj, errBack, canvas, context);
}

function buttonsEvents(video, videoObj, errBack, canvas, context){

    $("#snap, #video").unbind("click");
    $("#snap, #video").on("click", function(evt){
        $("#canvas").removeClass("hide");
        context.drawImage(video, 0, 0, 300, 225);
        $("#fotoImgBase64").val(canvas.toDataURL());
    }); 
    
    $("#cancel-snap-refresh").unbind("click");
    $("#cancel-snap-refresh").on("click", function(evt){
        $("#canvas").addClass("hide");
        $("#cancel-snap-refresh").addClass("hide");
        $("#ImgTalentoHumanoFoto").removeClass("hide");
        $("#snap-refresh").removeClass("hide");
        $("#fotoImgBase64").val("data:image/png;base64");
    });
    
    $("#snap-refresh").unbind("click");
    $("#snap-refresh").on("click", function(evt){
        configCamera();
        $("#canvas").removeClass("hide");
        $("#cancel-snap-refresh").removeClass("hide");
        $("#ImgTalentoHumanoFoto").addClass("hide");
        $("#snap-refresh").addClass("hide");
        $("#fotoImgBase64").val("");
    });
}
